# SOScrollViewInfinite

Are you too facing the same problem of displaying large amount of data on small screens of iOS devices?

Yes?

First of all, don’t worry because you’re not alone. We also had this same problem while we were working on a project. The project app required to display a large amount of data which seemed to be impossible at first.

But because of great teamwork, we finally learned about UIScrollView and how, with the help of UIScrollView, displayed this large amount of data efficiently in iOS devices.

Thus, we’ve written an article on the [UIScrollView Tutorial](https://www.spaceotechnologies.com/ios-tutorial-uiscrollview-integration/) to share with you about integrating and implementing it for manage large amount of data on screen.

However, this is just a simple demo, but if you have any idea for iOS app that also needs large amount of data to be handled and displayed efficiently, you can contact Space-O Technologies - An [iPhone app development company](https://www.spaceotechnologies.com/iphone-app-development/) for the same.